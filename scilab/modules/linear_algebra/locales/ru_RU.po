# Russian translation for scilab
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the scilab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: <localization@lists.scilab.org>\n"
"POT-Creation-Date: 2013-04-16 17:44+0100\n"
"PO-Revision-Date: 2021-07-11 01:00+0000\n"
"Last-Translator: Stanislav V. Kroter <krotersv@gmail.com>\n"
"Language-Team: Russian <ru@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 1b66c075b8638845e61f40eb9036fabeaa01f591)\n"

#, c-format
msgid "%s: Can not read input argument #%d.\n"
msgstr "%s: Не могу прочитать входной аргумент №%d.\n"

#, c-format
msgid "%s: Wrong type for argument #%d: Real or complex matrix expected.\n"
msgstr ""
"%s: Неверный тип аргумента №%d: ожидалась вещественная или комплексная "
"матрица.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: String or integer expected.\n"
msgstr ""
"%s: Неверный тип входного аргумента №%d: ожидалось строковое или "
"целочисленное значение.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: string expected.\n"
msgstr "%s: Неверный размер входного параметра №%d: ожидалась строка.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: %s, %s, %s, or %s expected.\n"
msgstr ""
"%s: Неверное значение входного аргумента №%d: ожидалось %s, %s, %s или %s.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: %s, %s, %s or %s expected.\n"
msgstr ""
"%s: Неверное значение входного аргумента №%d: ожидалось %s, %s, %s или %s.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A real expected.\n"
msgstr ""
"%s: Неверный тип входного аргумента №%d: ожидалось вещественное число.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: A real scalar expected.\n"
msgstr ""
"%s: Неверный размер входного аргумента №%d: ожидается вещественный скаляр.\n"

#, c-format
msgid "%s: Wrong number of output argument(s): %d expected.\n"
msgstr "%s: Неверное количество выходных аргументов: ожидалось %d.\n"

#, c-format
msgid "%s: Arguments %d and %d must have equal dimensions.\n"
msgstr "%s: Аргументы %d и %d должны иметь одинаковые размерности.\n"

#, c-format
msgid "%s: Wrong type for argument %d: Square matrix expected.\n"
msgstr "%s: Неверный тип аргумента %d: ожидалась квадратная матрица.\n"

#, c-format
msgid "%s: Cannot allocate more memory.\n"
msgstr "%s: Не удалось выделить больше памяти.\n"

#, c-format
msgid "%s: LAPACK error n%d.\n"
msgstr "%s: Ошибка LAPACK №%d.\n"

#, c-format
msgid "%s: Wrong number of output argument(s): %d to %d expected.\n"
msgstr "%s: Неверное количество выходных аргументов: ожидалось от %d до %d.\n"

#, c-format
msgid "%s: Wrong type for argument %d: Real or complex matrix expected.\n"
msgstr ""
"%s: Неверный тип аргумента %d: ожидалась вещественная или комплексная "
"матрица.\n"

#, c-format
msgid "%s: Wrong type for argument %d: A scalar expected.\n"
msgstr "%s: Неверный тип входного аргумента %d: ожидался скаляр.\n"

#, c-format
msgid "%s: Wrong value for argument %d: Must not contain NaN or Inf.\n"
msgstr "%s: Неверное значение аргумента %d: не должно быть NaN или Inf.\n"

#, c-format
msgid "%s: Allocation failed.\n"
msgstr "%s: Распределение не удалось.\n"

#, c-format
msgid "%s: Non convergence in QR steps.\n"
msgstr "%s: Нет сходимости в шагах QR.\n"

#, c-format
msgid "%s: Matrix is not positive definite.\n"
msgstr "%s: Матрица не является положительно определённой.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: Square matrix expected.\n"
msgstr ""
"%s: Неверный тип входного параметра №%d: ожидалась квадратная матрица.\n"

#, c-format
msgid "%s: Size varying argument a*eye(), (arg %d) not allowed here.\n"
msgstr ""
"%s: Здесь не разрешено изменение размера аргумента a*eye(), (arg %d).\n"

#, c-format
msgid "%s: Argument %d: Square matrix expected. Please use pinv() otherwise.\n"
msgstr ""

msgid "Warning :\n"
msgstr "Предупреждение:\n"

#, c-format
msgid "matrix is close to singular or badly scaled. rcond = %1.4E\n"
msgstr "матрица близка к сингулярной или плохо масштабирована. rcond = %1.4E\n"

#, c-format
msgid "%s: Problem is singular.\n"
msgstr "%s: Задача вырождена.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A Real expected.\n"
msgstr ""
"%s: Неверный тип входного параметра №%d: ожидалось вещественное число.\n"

#, c-format
msgid "%s: %s and %s must have equal number of rows.\n"
msgstr "%s: %s и %s должны иметь одинаковое количество строк.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: Real scalar expected.\n"
msgstr ""
"%s: Неверный тип входного аргумента №%d: ожидался вещественный скаляр.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A real or a string expected.\n"
msgstr ""
"%s: Неверный тип входного параметра №%d: ожидалось вещественное число или "
"строка.\n"

#, c-format
msgid "%s: Arg %d and arg %d must have equal dimensions.\n"
msgstr "%s: Аргумент %d и аргумент %d должны иметь одинаковые размеры.\n"

#, c-format
msgid "%s: Wrong number of output argument(s): %d or %d expected.\n"
msgstr "%s: Неверное количество выходных аргументов: ожидалось %d или %d.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: Real matrix expected.\n"
msgstr ""
"%s: Неверный тип входного параметра №%d: ожидалась матрица вещественных "
"чисел.\n"

#, c-format
msgid "%s: Subroutine not found: %s\n"
msgstr "%s: Подпрограмма не найдена: %s\n"

#, c-format
msgid "%s: Schur exit with state %d\n"
msgstr "%s: Schur вышел с состоянием %d\n"

#, c-format
msgid "%s: Wrong value for input argument %d: Must not contain NaN or Inf.\n"
msgstr ""
"%s: Неверное значение входного аргумента %d: недопустимы NaN или Inf.\n"

#, c-format
msgid ""
"%s: On entry to ZGEEV parameter number  3 had an illegal value (lapack "
"library problem).\n"
msgstr ""
"%s: На входе в ZGEEV у параметра номер 3 было недопустимое значение "
"(проблема библиотеки lapack).\n"

#, c-format
msgid ""
"%s: Convergence problem, %d off-diagonal elements of an intermediate "
"tridiagonal form did not converge to zero.\n"
msgstr ""
"%s: Проблема сходимости, %d недиагональных элементов промежуточной "
"трёхдиагональной формы не сходятся к нулю.\n"

#, c-format
msgid ""
"%s: On entry to ZHEEV parameter number  3 had an illegal value (lapack "
"library problem).\n"
msgstr ""
"%s: На входе в ZHEEV у параметра номер 3 было недопустимое значение "
"(проблема библиотеки lapack).\n"

#, c-format
msgid ""
"%s: The QR algorithm failed to compute all the eigenvalues, and no "
"eigenvectors have been computed. Elements and %d+1:N of W contain "
"eigenvalues which have converged.\n"
msgstr ""
"%s: Алгоритм QR не смог вычислить все собственные числа, и ни один вектор "
"собственных чисел не был вычислен. Элементы и %d+1:N в W содержат "
"собственные числа, которые сошлись.\n"

#, c-format
msgid ""
"%s: The QR algorithm failed to compute all the eigenvalues, and no "
"eigenvectors have been computed. Elements and %d+1:N of WR and WI contain "
"eigenvalues which have converged.\n"
msgstr ""
"%s: Алгоритм QR не смог вычислить все собственные числа, и ни один вектор "
"собственных чисел не был вычислен. Элементы и %d+1:N в WR и WI содержат "
"собственные числа, которые сошлись.\n"

msgid "Non convergence in the QZ algorithm.\n"
msgstr "В алгоритме QZ не схождения.\n"

#, c-format
msgid "The top %d  x %d blocks may not be in generalized Schur form.\n"
msgstr "Вершина %d  x %d блоков не может быть в обобщённой форме Шура.\n"

#, c-format
msgid "%s: The QZ iteration failed in DGGEV.\n"
msgstr "%s: Итерация QZ провалена в DGGEV.\n"

#, c-format
msgid "%s: Other than QZ iteration failed in DHGEQZ.\n"
msgstr "%s: Отличная от QZ итерация провалена в DHGEQZ.\n"

#, c-format
msgid "%s: Error return from DTGEVC.\n"
msgstr "%s: Возврат с ошибкой из DTGEVC.\n"

#, c-format
msgid "%s: Wrong number of output argument(s): At least %d expected.\n"
msgstr "%s:  Неверное количество выходных аргументов: ожидалось не менее %d.\n"

#, c-format
msgid "%s: Convergence problem...\n"
msgstr "%s: Проблема сходимости...\n"

#, c-format
msgid "Argument %d in dgees had an illegal value.\n"
msgstr "У аргумента %d в dgees недопустимое значение.\n"

msgid "The QR algorithm failed to compute all the eigenvalues.\n"
msgstr "Алгоритм QR не смог вычислить все собственные числа.\n"

msgid ""
"The eigenvalues could not be reordered because some eigenvalues were too "
"close to separate (the problem is very ill-conditioned).\n"
msgstr ""
"Собственные числа нельзя переставить поскольку некоторые собственные числа "
"слишком близки для разделения (задача очень плохо обусловлена).\n"

msgid ""
"After reordering, roundoff changed values of some complex eigenvalues so "
"that leading eigenvalues in the Schur form no longer satisfy SELECT=.TRUE. "
"This could also be caused by underflow due to scaling.\n"
msgstr ""
"После перестройки округление изменило значения некоторых комплексных "
"собственных значений так, что первые собственные значения в форме Шура "
"больше не удовлетворяют SELECT=.TRUE. Это может быть также из-за обращения в "
"машинный нуль из-за масштабирования.\n"

#, c-format
msgid "Argument %d in zgees had an illegal value.\n"
msgstr "У аргумента %d в zgees недопустимое значение.\n"

#, c-format
msgid "Argument %d in dgges had an illegal value.\n"
msgstr "У аргумента %d в dgges недопустимое значение.\n"

msgid "The QZ iteration failed. (A,E) are not in Schur form.\n"
msgstr "Итерация QZ провалена. (A,E) не в форме Шура.\n"

msgid "Other than QZ iteration failed in DHGEQZ.\n"
msgstr "Отличная от QZ итерация провалена в DHGEQZ.\n"

msgid "Reordering failed in DTGSEN.\n"
msgstr "Провалена перестройка в DTGSEN.\n"

#, c-format
msgid "Argument %d in zgges had an illegal value.\n"
msgstr "У аргумента %d в zgges недопустимое значение.\n"

msgid "Other than QZ iteration failed in ZHGEQZ.\n"
msgstr "Отличная от QZ итерация провалена в ZHGEQZ.\n"

msgid "Reordering failed in ZTGSEN.\n"
msgstr "Провалена перестройка в ZTGSEN.\n"

#, c-format
msgid "%s: Argument #%d: Decimal or complex number(s) expected.\n"
msgstr ""
"%s: Аргумент №%d: ожидалось десятичное или комплексное число (числа).\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: Polynomial expected."
msgstr "%s: Неверный тип входного аргумента №%d: ожидался тип polynomial."

#, c-format
msgid "%s: Wrong value for input argument #%d: must not contain %s or %s.\n"
msgstr ""
"%s: Неверное значение входного аргумента №%d: не должно содержать %s или "
"%s.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A matrix expected.\n"
msgstr "%s: Неверный тип входного аргумента №%d: ожидалась матрица.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A square matrix expected.\n"
msgstr ""
"%s: Неверный тип входного аргумента №%d: ожидалась квадратная матрица.\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A scalar or a string expected.\n"
msgstr "%s: Неверный тип входного параметра №%d: ожидался скаляр или строка.\n"

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: must be %d, %d, %s, '%s' or '%s'.\n"
msgstr ""
"%s: Неверное значение входного аргумента №%d: должно быть %d, %d, %s, '%s' "
"или '%s'.\n"

#, c-format
msgid "%s: Wrong number of input arguments: %d to %d expected.\n"
msgstr "%s: Неверное количество входных аргументов: ожидалось от %d до %d.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: A scalar expected.\n"
msgstr "%s: Неверный размер входного параметра №%d: ожидался скаляр.\n"

#, c-format
msgid ""
"%s: Wrong values for input argument #%d: Non-negative integers expected.\n"
msgstr ""
"%s: Неверные значения входного параметра №%d: ожидались неотрицательные "
"целые числа.\n"

#, c-format
msgid "%s : Wrong number of input arguments : %d to %d expected.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: A full or sparse square matrix or a "
"function expected"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: A positive integer expected if the "
"first input argument is a function."
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: A structure expected"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument: If A is a matrix, use opts with tol, "
"maxiter, ncv, resid, cholB"
msgstr ""

#
# File: modules/linear_algebra/macros/eigs.sci, line: 163
#, c-format
msgid ""
"%s: Wrong type for input argument: If A is a function, use opts with tol, "
"maxiter, ncv, resid, cholB, issym"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: An empty matrix or full or sparse "
"square matrix expected.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong dimension for input argument #%d: B must have the same size as A.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: A scalar expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: k must be a positive integer.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: For real symmetric problems, k must "
"be an integer in the range 1 to N - 1.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: For real non symmetric or complex "
"problems, k must be an integer in the range 1 to N - 2.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: string expected.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: Unrecognized sigma value.\n"
" Sigma must be one of '%s', '%s', '%s', '%s' or '%s'.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: Unrecognized sigma value.\n"
" Sigma must be one of '%s', '%s', '%s', '%s', '%s' or '%s'.\n"
msgstr ""

#
# File: modules/linear_algebra/macros/eigs.sci, line: 291
# File: modules/linear_algebra/macros/eigs.sci, line: 751
#, c-format
msgid "%s: Invalid sigma value for complex or non symmetric problem.\n"
msgstr ""

#
# File: modules/linear_algebra/macros/eigs.sci, line: 294
# File: modules/linear_algebra/macros/eigs.sci, line: 754
#, c-format
msgid "%s: Invalid sigma value for real symmetric problem.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: a real scalar or a string expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: %s must be a scalar.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: %s must be an integer positive "
"value.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: %s must be a real scalar.\n"
msgstr ""

#, c-format
msgid "%s: Wrong dimension for input argument #%d: %s must be 1 by 1 size.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: %s must be an integer scalar.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: For real symmetric problems, NCV "
"must be k < NCV <= N.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: For real non symmetric problems, NCV "
"must be k + 2 < NCV < N.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: For complex problems, NCV must be k "
"+ 1 < NCV <= N.\n"
msgstr ""

#, c-format
msgid "%s: Wrong value for input argument #%d: %s must be %s or %s.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: %s must be an integer scalar or a "
"boolean.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: A real or complex matrix expected.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong dimension for input argument #%d: Start vector %s must be N by 1.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: Start vector %s must be real for real "
"problems.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: Start vector %s must be complex for "
"complex problems.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: if opts.cholB is true, B must be "
"upper triangular.\n"
msgstr ""

#
# File: modules/linear_algebra/macros/eigs.sci, line: 435
# File: modules/linear_algebra/macros/eigs.sci, line: 895
#, c-format
msgid ""
"%s: Impossible to use the Cholesky factorization with complex sparse "
"matrices.\n"
msgstr ""

#
# File: modules/linear_algebra/macros/eigs.sci, line: 506
# File: modules/linear_algebra/macros/eigs.sci, line: 512
# File: modules/linear_algebra/macros/eigs.sci, line: 519
# File: modules/linear_algebra/macros/eigs.sci, line: 591
# File: modules/linear_algebra/macros/eigs.sci, line: 609
# File: modules/linear_algebra/macros/eigs.sci, line: 646
# File: modules/linear_algebra/macros/eigs.sci, line: 953
# File: modules/linear_algebra/macros/eigs.sci, line: 958
# File: modules/linear_algebra/macros/eigs.sci, line: 964
# File: modules/linear_algebra/macros/eigs.sci, line: 1062
# File: modules/linear_algebra/macros/eigs.sci, line: 1079
# File: modules/linear_algebra/macros/eigs.sci, line: 1115
#, c-format
msgid "%s: Error with %s: info = %d.\n"
msgstr ""

#
# File: modules/linear_algebra/macros/eigs.sci, line: 573
# File: modules/linear_algebra/macros/eigs.sci, line: 575
# File: modules/linear_algebra/macros/eigs.sci, line: 578
# File: modules/linear_algebra/macros/eigs.sci, line: 1040
# File: modules/linear_algebra/macros/eigs.sci, line: 1042
# File: modules/linear_algebra/macros/eigs.sci, line: 1045
#, c-format
msgid "%s: Error with %s: unknown mode returned.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: n must be a positive integer.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: For real symmetric problems, k must "
"be in the range 1 to N - 1.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: For real non symmetric or complex "
"problems, k must be in the range 1 to N - 2.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: a scalar expected.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type for input argument #%d: a string expected.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: Unrecognized sigma value.\n"
" Sigma must be one of %s, %s, %s, %s or %s.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: Unrecognized sigma value.\n"
" Sigma must be one of %s, %s, %s, %s, %s or %s.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong dimension for input argument #%d: %s must be an integer scalar.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong dimension for input argument #%d: Start vector opts.resid must be "
"N by 1.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: Start vector opts.resid must be real "
"for real problems.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: Start vector opts.resid must be "
"complex for complex problems.\n"
msgstr ""

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: n does not match rows number of "
"matrix A.\n"
msgstr ""

#, c-format
msgid "%s: Wrong type or value for input arguments.\n"
msgstr ""

#, c-format
msgid "%s: Wrong size for input argument #%d: A column vector expected.\n"
msgstr "%s: Неверный размер входного параметра №%d: ожидался вектор-столбец.\n"

#, c-format
msgid "%s: Wrong type for argument %d: Decimal or complex numbers expected.\n"
msgstr ""
"%s: Неверный тип аргумента %d: ожидались десятичные или комплексные числа.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: Column vector expected.\n"
msgstr "%s: Неверный размер входного аргумента №%d: ожидался вектор-столбец.\n"

#, c-format
msgid "%s: Incompatible input arguments #%d and #%d: Same sizes expected.\n"
msgstr ""
"%s: Несовместимые входные параметры №%d и №%d: ожидались одинаковые "
"размеры.\n"

#, c-format
msgid "%s: SVD and QR not implemented in sparse.\n"
msgstr "%s: Алгоритмы SVD и QR не реализованы для разреженных матриц.\n"

#, c-format
msgid "%s: This feature has not been implemented.\n"
msgstr "%s: Эта функциональность не реализована.\n"

msgid "Conflicting linear constraints."
msgstr "Конфликтующие линейные  ограничения."

msgid "Recomputing initial guess"
msgstr "Пересчёт исходного предположения"

#, c-format
msgid "Possible Conflicting linear constraints, error in the order of %s"
msgstr "Возможно конфликтующие линейные ограничения, ошибка в порядке %s"

#, c-format
msgid "%s: Wrong number of input arguments: %d or %d expected.\n"
msgstr "%s: Неверное количество входных параметров: ожидалось %d или %d.\n"

#, c-format
msgid "%s: Argument #%d: Square matrix expected.\n"
msgstr "%s: Аргумент №%d: ожидалась квадратная матрица.\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"
msgstr ""
"%s: Неверное значение входного параметра №%d: должно быть из множества "
"{%s}.\n"

#, c-format
msgid "%s: Singular pencil."
msgstr "%s: Сингулярный пучок."

#, c-format
msgid ""
"%s: Function not defined for type '%s'. Check argument or define function %s."
msgstr ""
"%s: Функция не определена для типа '%s'. Проверьте аргумент или определите "
"функцию %s."

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: Non-negative integer expected.\n"
msgstr ""
"%s: Неверное значение входного параметра №%d: ожидалось неотрицательное "
"целое.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: A square matrix expected.\n"
msgstr ""
"%s: Неверный размер входного параметра №%d: ожидалась квадратная матрица.\n"

#, c-format
msgid ""
"%s: Wrong values for input argument #%d: Non-negative scalar expected.\n"
msgstr ""
"%s: Неверные значения входного параметра №%d: ожидался неотрицательный "
"скаляр.\n"

#, c-format
msgid "%s: Wrong size for input argument #%d: Symmetric expected"
msgstr "%s: Неверный размер входного параметра №%d: ожидался симметричный"

#, c-format
msgid "%s: Wrong value for input argument #%d: Not semi-definite positive"
msgstr ""
"%s: Неверное значение входного параметра №%d: ожидалось неотрицательно "
"определённое"

#, c-format
msgid "%s: Wrong type for input argument #%d.\n"
msgstr "%s: Неверный тип входного параметра №%d.\n"

#, c-format
msgid ""
"%s: Wrong value for input argument #%d: Requested rank is greater than "
"matrix dimension."
msgstr ""
"%s: Неверное значение входного параметра №%d: запрошенный ранг больше "
"размерности матрицы."
